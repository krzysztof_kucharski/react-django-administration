import styled from "styled-components";

export const StyledForgotPasswordPage = styled.div`
  overflow: hidden;
  width: 100%;
  height: 100%;
  box-sizing: border-box;
  display: flex;
  align-items: center;
  justify-content: center;
  
  @media (max-width: 600px) {
    margin-top: 0;
  }

  .wrapperEmailSent {
    height: 100%;
    box-sizing: border-box;
    display: flex;
    align-items: center;
    justify-content: center;
  }
`;