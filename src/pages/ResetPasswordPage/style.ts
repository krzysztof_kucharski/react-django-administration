import styled from "styled-components";

export const StyledResetPasswordPage = styled.div`
  overflow: hidden;
  box-sizing: border-box;
  width: 100%;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;

  @media (max-width: 600px) {
    margin-top: 0;
  }
`;