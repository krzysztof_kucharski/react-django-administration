export {default as authReducer} from 'appRedux/reducers/api/account/authSlice';
export * from 'appRedux/reducers/api/account/authSlice';

export {default as registrationReducer} from 'appRedux/reducers/api/account/registrationSlice';
export * from 'appRedux/reducers/api/account/registrationSlice';

export {default as confirmReducer} from 'appRedux/reducers/api/account/confirmSlice';
export * from 'appRedux/reducers/api/account/confirmSlice';

export {default as forgotPasswordReducer} from 'appRedux/reducers/api/account/forgotPasswordSlice';
export * from 'appRedux/reducers/api/account/forgotPasswordSlice';

export {default as resetPasswordReducer} from 'appRedux/reducers/api/account/resetPasswordSlice';
export * from 'appRedux/reducers/api/account/resetPasswordSlice';