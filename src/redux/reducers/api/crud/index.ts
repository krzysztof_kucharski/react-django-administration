export {default as listModelsReducer} from 'appRedux/reducers/api/crud/listModelsSlice';
export * from 'appRedux/reducers/api/crud/listModelsSlice';

export {default as modelDataReducer} from 'appRedux/reducers/api/crud/modelDataSlice';
export * from 'appRedux/reducers/api/crud/modelDataSlice';

export {default as addItemToTableReducer} from 'appRedux/reducers/api/crud/addItemToTableSlice';
export * from 'appRedux/reducers/api/crud/addItemToTableSlice';