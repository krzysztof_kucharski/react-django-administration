export const deleteAccountButtonTheme = {
    width: "130px",
    height: "40px",
    background: "#d21f31",
    disabledBackground: "rgba(47,47,47,0.43)",
    hoverBackground: "#a91926",
    border: "none",
    borderRadius: "6px",
    margin: "0px 0px 0px 10px",

    text: {
        textAlign: "center",
        fontSize: "14px",
        textColor: "#ffffff",
        fontWeight: "700",
        disabledTextColor: "rgba(255,255,255,0.20)"
    }
};
