export const logoutButtonTheme = {
    width: "130px",
    height: "40px",
    background: "#0073ff",
    disabledBackground: "rgba(47,47,47,0.43)",
    hoverBackground: "#363636",
    border: "none",
    borderRadius: "10px",

    text: {
        fontSize: "14px",
        textColor: "#ffffff",
        disabledTextColor: "rgba(255,255,255,0.20)",
        textAlign: "center"
    }
};