import styled from "styled-components";

export const StyledWaitingComponent = styled.div`
  position: absolute;
  bottom: 0;
  right: 0;
  width: 100px;
  height: 40px;
`;